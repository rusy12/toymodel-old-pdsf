void rec_jets()
{
  gSystem->Load("~/jet_analysis/toymodel/Production/libThrm.so");
  gSystem->Load("~/jet_analysis/toymodel/Analysis/libThrmAna.so");
  gSystem->Load("~/jet_analysis/toymodel/Unfolding/libtoyUnfold.so");
  TStopwatch timer;
  timer.Start();

  TString sradius = gSystem->Getenv("RADIUS");
  TString spTcut = gSystem->Getenv("PTCUT");

  Double_t radius = sradius.Atof();
  Double_t pTcut = spTcut.Atof();

  TString wrkdir = gSystem->Getenv("INPUTDIR");

  ThrmJetReco *reco = new ThrmJetReco(wrkdir, radius, pTcut);
  reco->RunJetReco();
  delete reco;

  timer.Stop();
  timer.Print();
}
