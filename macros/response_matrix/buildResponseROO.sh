#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (deltapT | BG_dete)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

export RPARAM=0.2
export RMATRIX_TYPE
JETTYPE="pythia"
#JETTYPE="sp"
SUFF="_normal"


export PATH_TO_DELTA_PT_HISTOGRAMS="$HOME/jet_analysis/toymodel/DataOut/$JETTYPE/jet_plus_bg/charged_R${RPARAM}"
export RM_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jet_plus_bg/charrged_R${RPARAM}/rmatrix${SUFF}"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFF}"
export PRIOR_PATH="$HOME/jet_analysis/STARJet/out/prior"

for PTTHRESH in 5 #7
do
   export PTTHRESH
	#root -l make_epsilon.C -q -b
	for PRIOR in 2 3 6 #0: scaled_pp, 1: flat, 2: biased pythia, 3: pT^(-5), 4:pT^(-6) 5: pT^(-7) 6:(pT-2)^(-6)
	do
		export PRIOR
		root -l buildResponseROO.C -q -b
	done
done
