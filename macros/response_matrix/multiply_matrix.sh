#!/bin/bash
export JETTYPE="pythia"
export REVERSE=0 #0: BGxDete 1: DetexBG
export CENTRAL=1

if [ $CENTRAL -eq 1 ]; then
PTLEADCUTS="5 6 7"
else
PTLEADCUTS="4 5 6"
fi

for RPARAM in 0.3 0.2 0.4 
do
export RPARAM
for PTTHRESH in `echo $PTLEADCUTS`
   do
   export PTTHRESH
   root -l multiply_matrix.C -q
done
done
