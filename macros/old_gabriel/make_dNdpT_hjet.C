void make_dNdpT_hjet()
{
  TStopwatch timer;
  timer.Start();
  
  TString wrkdir = gSystem->Getenv("WRKDIR");
  
  ThrmAnadNdpT *dNdpT = new ThrmAnadNdpT(wrkdir);
  dNdpT->SetMultiplicity(2000);
  dNdpT->RunHadronJet();
  delete dNdpT;
  
  timer.Stop();
  timer.Print();
}
