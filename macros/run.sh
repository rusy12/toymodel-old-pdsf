#!/bin/bash
WPATH="../DataOut/sp_test/jetonly/1k_charged"
#WPATH="../DataOut/sp_test/jetonly/1k_full"
export OUTPUTDIR=$WPATH
export INPUTDIR=$WPATH
export WRKDIR=$WPATH

export NEVENTS=1E0

export NBIN=984 # 1259 # 
#export NBIN=1259 
export SIGMA_NBIN=0
export MULTIPLICITY=1333 #2000 # 4800 #
#export MULTIPLICITY=2000
export SIGMA_MULTIPLICITY=0
export COLLIDER="RHIC_CHARGED" # RHIC # LHC # 
#export COLLIDER="RHIC"   

export ACUT=0.2
#export ACUT=0.0
export PTCUT=0.2
export RADIUS=0.3
export BKGDPTCUT=0.2

export JETONLY=1 #only hard jet distribution
export BOLTZMANN=0 #only background
export PYTHIAHARDJET=1 #use pythia jets with pTleading cut as the hard jet distribution
export EFFICORR=0

mkdir -p $OUTPUTDIR

root -l produce_data.C 

root -b -q -l make_dNdpT.C
root -b -q -l rec_jets.C
root -b -q -l make_ntuple.C
root -b -q -l make_histos.C
root -q -b -l make_histos_deltapT.C
