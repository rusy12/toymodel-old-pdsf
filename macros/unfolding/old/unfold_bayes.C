void unfold_toymodel()
{
  TStopwatch timer;
  timer.Start();
 
  TString str;
  TString prior_type[]={"pp_scaled","flat","pythia","powlaw5","powlaw6","powlaw4","powlaw3"};

  Int_t priorNo= atoi(gSystem->Getenv("PRIOR"));
  TString data_path = gSystem->Getenv("DATA_PATH");
  TString true_path = gSystem->Getenv("TRUE_PATH");
  TString prior_path = gSystem->Getenv("PRIOR_PATH");
  TString rmatrix_path = gSystem->Getenv("RMATRIX_PATH");

  Double_t pTcut = atof(gSystem->Getenv("PTCUT"));
  Double_t pTthresh = atof(gSystem->Getenv("PTTHRESH"));
  Double_t R = atof(gSystem->Getenv("RPARAM"));
  Int_t nbins = atoi(gSystem->Getenv("NBINS"));
  Int_t niterations =atoi(gSystem->Getenv("NITER")); 
 
  str = Form("%s/histos_jets_R%.1lf_pTcut%.1lf.root", data_path.Data(),R, pTcut);
  TFile *finput = new TFile(str.Data(), "OPEN");
  
  TH1D *hFullSpectrum = (TH1D*)finput->Get("hDirectSpectrum");
  TH2D *hDSpTleading = (TH2D*)finput->Get("fhDSpTleading");
  Int_t firstbin = hDSpTleading->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hDSpTleading->GetNbinsX();
  TH1D *hSignalSpectrum = hDSpTleading->ProjectionY("hSignalSpectrum", firstbin, lastbin);
  hSignalSpectrum->Sumw2();
  Double_t int_signal=hSignalSpectrum->Integral();
  
  //str = Form("%s/response_matrix_deltapT_R%.1lf.root", rmatrix_path.Data(),R);
  str = Form("%s/response_matrix_gaussian.root", rmatrix_path.Data());
  TFile *frmatrix = new TFile(str.Data(), "OPEN");
  TH2D *rmatrix = (TH2D*)frmatrix->Get("hResponse_1E9");

  // MAKE SURE DIM = nbins
  // for time constrain purpose
  Int_t nbinsx;
  nbinsx = hFullSpectrum->GetNbinsX();
  if(nbinsx != nbins) hFullSpectrum->Rebin(nbinsx / nbins);
  nbinsx = hSignalSpectrum->GetNbinsX();
  if(nbinsx != nbins) hSignalSpectrum->Rebin(nbinsx / nbins);

  // MAKE SURE DIM = nbins x nbins
  Int_t nbinsy;
  nbinsx = rmatrix->GetNbinsX();
  nbinsy = rmatrix->GetNbinsY();
  if(nbinsx != nbins && nbinsy != nbins) rmatrix->Rebin2D(nbinsx / nbins, nbinsy / nbins);
  // TRANSPOSING R-MATRIX 
  TH2D *htemp = (TH2D*)rmatrix->Clone("htemp");
  rmatrix->Reset("MICE");
  rmatrix->GetXaxis()->SetTitle("p_{T}^{truth} (GeV/c)");
  rmatrix->GetYaxis()->SetTitle("p_{T}^{measured} (GeV/c)");
  for(Int_t binx = 1; binx <= nbins; binx++)
    for(Int_t biny = 1; biny <= nbins; biny++)
      rmatrix->SetBinContent(biny, binx, htemp->GetBinContent(binx, biny));
  
  // PRIOR
  TH1D *hprior = (TH1D*)hFullSpectrum->Clone("hprior");
  hprior->SetName("hprior");
  hprior->Reset("MICE");
if(priorNo==0){
  str = Form("%s/histos_jets_R%.1lf_pTcut0.2.root", true_path.Data(),R);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  TH1D *hPtReco = hPtRecpTleadingPrior->ProjectionY("hSignalSpectrum", firstbin, lastbin);
  }
else if (priorNo==2){
  str = Form("%s/histos_pythiajet_R%.1lf.root", prior_path.Data(),R);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPrior2d = (TH2D*)fprior->Get("hpT_pTlead");
  Int_t firstbin = hPrior2d->GetYaxis()->FindBin(pTthresh);
//cout<<"firstbin "<<firstbin<<endl;
  Int_t lastbin = hPrior2d->GetNbinsY();
//cout<<"lastbin "<<lastbin<<endl;
  TH1D *hPtReco = (TH1D*)hPrior2d->ProjectionX("prior_pythia", firstbin, lastbin);
  }

else{
  str = Form("%s/histos_prior.root", prior_path.Data());
  TFile *finput2 = new TFile(str.Data(), "OPEN");
  str=Form("hprior_%s",prior_type[priorNo].Data());
  TH2D *hprior2d = (TH2D*)finput2->Get(str.Data());
  firstbin = hprior2d->GetYaxis()->FindBin(pTthresh);
  lastbin = firstbin;
  TString priorName=Form("prior_%i",priorNo);
  TH1D *hPtReco = hprior2d->ProjectionX(priorName,firstbin,lastbin,"e");
  }

  hPtReco->Sumw2();
  Double_t int_prior=hPtReco->Integral();

  for(Int_t bin = 1; bin <= hPtReco->GetNbinsX(); bin++)
    {
      Double_t pT = hPtReco->GetBinCenter(bin);
      Double_t yield = hPtReco->GetBinContent(bin);
      Double_t error = hPtReco->GetBinError(bin);

      hprior->Fill(pT, yield*(int_signal/int_prior));
    }
  
  str = Form("%s", data_path.Data());
  
  UnfoldBayes *bayes = new UnfoldBayes(niterations, Form("%s/Unfolded_R%.1lf_%ibins/%s/unfolded_SignalSpectrum_R%.1lf_pTthresh%.1lf.root", str.Data(), R, nbins, prior_type[priorNo].Data(), R, pTthresh));
  bayes->SetHistograms(hSignalSpectrum, hprior, rmatrix);
  bayes->Unfold();
  delete bayes;

  timer.Stop();
  timer.Print();
}

