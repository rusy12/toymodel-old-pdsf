#!/bin/bash

# FOR PRIOR DISTRIBUTION
cd ~/Doutorado/toymodel/software/macros

for COLLIDER in 'RHIC' 'LHC'
  do
  for TYPE in 'pythia' 'sp'
    do
    WRKDIR="~/Doutorado/toymodel/data/${COLLIDER}/inclusive/${TYPE}/full"
    export PRIOR_PATH="~/Doutorado/toymodel/data/${COLLIDER}/inclusive/${TYPE}/full"
    export DATA_PATH=${WRKDIR}
    export RMATRIX_PATH=${WRKDIR}
    
    root -b -l -q ../Unfolding/AnaMacros/unfold_toymodel_nocut.C
  done
done
