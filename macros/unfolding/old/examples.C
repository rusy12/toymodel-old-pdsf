void examples()
{
  TStopwatch timer;
  timer.Start();

  UnfoldExample *example = new UnfoldExample();
  example->DeltaTruth();
  example->GausTruth();
  example->Pol1Truth();
  example->Pol2Truth();
  example->ExpoTruth();

  timer.Stop();
  timer.Print();
}
