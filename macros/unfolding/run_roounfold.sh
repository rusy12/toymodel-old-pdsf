#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BGD | dete | effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BGD effi
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

#prior_type=(truth flat pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex )
prior_type=(flat pythiadete pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)
COLLIDER="RHIC" # "LHC" # 
JET_TYPE="pythia" #pythia | sp 
#JET_TYPE="sp" #pythia | sp 
export SVD=0 # SVD unfolding instead of Bayes
#export NBINS=200
export NBINS="VAR"
export NITER=1 #number of iterations
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export BINCONTCUT=10 #minimal bin content in measured distribution
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
	export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
#export PTCUTOFF=0 #from witch pT to start with unfolding
EFFICORR=1 # do efficiency correction
SUF="_test" #output directory suffix
EFFI_UNFOLD=0 #unfold detector level jet spectrum for jet reco efficiency calculation
if [ $RMATRIX_TYPE == "effi" ]; then
	EFFI_UNFOLD=1
	BININGS="1"
else
	BININGS="0" #1 2 3 4"
fi
export EFFI_UNFOLD

for CENTRAL in 1 #central/peripheral collisons
do
if [ $CENTRAL -eq 1 ]; then
	CENTSUFF=""
	PTLEADCUTS="5 6 7"
	RAA=0.5 #RAA value used for generating the simulated spectrum - used only for desctription
else
	CENTSUFF="_peripheral"
	PTLEADCUTS="4 5 6"
	RAA=0.7 #RAA value used for generating the simulated spectrum - used only for desctription
fi

for BININGCH in `echo $BININGS`
do
export BININGCH 
for RPARAM in 0.3 #0.3 0.4
do
export RPARAM

for SYSSUFF in "_normal"  #"_AuAu" #"_normal" "_m5" "_p5" "_2u1g" #
do

TOYMODELPATH="$HOME/jet_analysis/toymodel"
WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}$CENTSUFF"
if [ ${EFFI_UNFOLD} -eq 1 ]; then
	WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/pyEmb_R${RPARAM}$CENTSUFF$SYSSUFF"
fi
export TRUE_PATH="$TOYMODELPATH/DataOut/sp/jetonly/charged_R${RPARAM}$CENTSUFF"
export PRIOR_PATH="~/jet_analysis/STARJet/out/MB/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH="$WRKDIR/rmatrix"
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${SYSSUFF}/epsilon" #path to jet reconstruction efficiency files
export RMATRIX_TYPE


if [ $EFFI_UNFOLD -eq 1 -o $RMATRIX_TYPE == "BG_sp" ]; then
	export EFFICORR=0 
else
	export EFFICORR
fi
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

if [ $SVD -eq 0 ]; then
UTYPE="Bayes"
else
UTYPE="SVD"
fi

#cd "$TOYMODELPATH/macros/unfolding"
for PTTHRESH in `echo $PTLEADCUTS`
do
  export PTTHRESH
if [ $EFFI_UNFOLD -eq 1 ]; then
	export PRIOR=3
	export NBINS=200
	OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_${RMATRIX_TYPE}/"${prior_type[$PRIOR]}
   #if [ $PTCUTOFF -gt 0 ]; then
	#	OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_Bayes_${NBINS}bins_pTcut${PTCUTOFF}/"${prior_type[$PRIOR]}
   #fi
   echo "creating directory: $OUT_DIR"
   mkdir -p $OUT_DIR
	export OUT_DIR

	  root -b -l -q unfold_roounfold.C

else
	for PRIOR in 2 #4 5 6 7 8 9 10 11 12 13 14 15
	do
	export PRIOR
	#OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${NBINS}bins/"${prior_type[$PRIOR]}
	if [ $SECONDUNFOLD -eq 0 ]; then
		OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA}${SUF}/"${prior_type[$PRIOR]}
	else
		OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_U2_initer${INPUTITER}/"${prior_type[$PRIOR]}
	fi
   #if [ $PTCUTOFF -gt 0 ]; then
	#	OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_Bayes_${NBINS}bins_pTcut${PTCUTOFF}/"${prior_type[$PRIOR]}
   #fi
   echo "creating directory: $OUT_DIR"
   mkdir -p $OUT_DIR
	export OUT_DIR

	if [ $NBINS == "VAR" ]; then  
     root -b -l -q unfold_roounfold_uneqbin.C
      else
     root -b -l -q unfold_roounfold.C
      fi
	done #prior
fi
done #pTleading
done #syssuf
done #R
done #bining
done #centrality
