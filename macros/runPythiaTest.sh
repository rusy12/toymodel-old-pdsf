#!/bin/bash

WPATH="$HOME/jet_analysis/toymodel/DataOut/test"


export OUTPUTDIR=$WPATH
export WRKDIR=$WPATH

export NEVENTS=500000

export PTCUT=0.2
export MAXRAP=1.0
export NJOBS=1
export EFFICORR=0 #apply tracking efficiency 
export PTSMEAR=0 #track pT smearing
export EFFTYPE="pp" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies
export DOSCALE=0 #rescale charge jets to the momentum of created full jet
export CENTRAL=1 #central or peripheral collisions

for RPARAM in 0.3 0.6
do
export RPARAM

   if [ $RPARAM == "0.2" ]; then
      AREACUT=0.09
   fi
   if [ $RPARAM == "0.3" ]; then
      AREACUT=0.2
   fi
   if [ $RPARAM == "0.4" ]; then
      AREACUT=0.4
   fi
   if [ $RPARAM == "0.5" ]; then
      AREACUT=0.65
   fi
   if [ $RPARAM == "0.6" ]; then
      AREACUT=0.8
   fi
export AREACUT

for CHARGED in 0 1 #charged jets only
do
export CHARGED
for JETFRAG in "g" #jet fragmentation: u | g
do
export JETFRAG
for WEIGHT in 0 1 # 0: flat, 1: pT^-5
do
export WEIGHT

root -l -q -b run_pythiaTest.C 

done
done 
done
done
