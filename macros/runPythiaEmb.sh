#!/bin/bash

WPATH="$HOME/jet_analysis/toymodel/DataOut/test"


export OUTPUTDIR=$WPATH
export WRKDIR=$WPATH

export NEVENTS=100

export PTCUT=0.2
export RPARAM=0.2
export MAXRAP=1.0
export AREACUT=0.09
export NJOBS=1

export CENTRAL=0 #central or peripheral collisions
export CHARGED=1 #charged jets only
export JETFRAG="2u1g" #jet fragmentation: u | g
export EFFICORR=1 #apply tracking efficiency 
export PTSMEAR=1 #track pT smearing
export EFFTYPE="pp" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies
export DOSCALE=0 #rescale charge jets to the momentum of created full jet
export TOFEFFI=1 #correc also for TOF+BEMC maching efficiency (for pp study)
export TOFEFF_INCREMENT=0 #increase/decrease TOF+BEMC matching efficiency for systematic studies

root -l -q -b run_pythiaEmb.C 
#done
