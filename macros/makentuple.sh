#!/bin/bash

export WRKDIR="../DataOut/hjet"

export PTCUT=0.2
export RADIUS=0.4

root -l -b make_ntuple.C -q
