#!/bin/bash
STARLIB_VER="SL15e"
BASEPATH="/global/homes/r/rusnak/jet_analysis/STARJet"
export STARJETBASEDIR=$BASEPATH 

export ROOUNFOLD="$BASEPATH/software_$STARLIB_VER/RooUnfold/v-trunk-custom"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$ROOUNFOLD"

export FASTJETDIR="/global/homes/r/rusnak/jet_analysis/STARJet/software_$STARLIB_VER/fastjet3"
export PATH="$PATH:$FASTJETDIR/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$FASTJETDIR/lib"

TOYPATH="/global/homes/r/rusnak/jet_analysis/toymodel"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYPATH/Production"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYPATH/Analysis"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYPATH/Unfolding"
