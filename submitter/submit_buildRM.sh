#!/bin/bash
BASEDIR="$HOME/jet_analysis/toymodel"
LOGDIR="$BASEDIR/submitter/log"
export WORKDIR="$BASEDIR/macros/response_matrix"
JET_TYPE="pythia" # pythia | sp
CENTRAL=1
if [ $CENTRAL -eq 1 ]; then
SUFF=""
PTLEADCUTS="5 6 7"
else
SUFF="_peripheral"
PTLEADCUTS="3 4 5"
fi

for RPARAM in 0.3 #0.3 0.4
do
export RPARAM
export PATH_TO_DELTA_PT_HISTOGRAMS="$BASEDIR/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}${SUFF}" #path to input histograms 
for PTLEAD in `echo $PTLEADCUTS`
do
	export PTLEAD
   NAME="buildRM_R${RPARAM}_pTl${PTLEAD}"
qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRM.sh 
done
done
