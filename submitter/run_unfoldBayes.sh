#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load toymodel/toymodel

BASEDIR=$HOME/jet_analysis/toymodel
MacroDIR=$BASEDIR/macros/unfolding

prior_type=(truth flat pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex )

if [ $CENTRAL -eq 1 ]; then
	CENTSUFF=""
	PTLEADCUTS="5 7"
else
	CENTSUFF="_peripheral"
	PTLEADCUTS="3 4 5"
fi


for PRIOR in 2 4 5 6 7 8 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:(pT-6) 7: levy 8: levy II
do
   OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_Bayes_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA}${SUF}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
	export OUT_DIR
   export PRIOR
   for PTTHRESH in `echo $PTLEADCUTS`
   do
   export PTTHRESH
 
cd $MacroDIR
if [ $NBINS == "VAR" ]; then
  root -b -l -q unfold_roounfold_uneqbin.C
else
  root -b -l -q unfold_roounfold.C
fi
done #pTlead
done #prior

