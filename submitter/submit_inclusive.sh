#!/bin/bash

export BASEDIR=$HOME/jet_analysis/toymodel
LOGDIR=$BASEDIR/submitter/log

export ANAONLY=0 # do not generate events, run only jet reconstruction again 

MEVENTS=10 #in millions
export COLLIDER="RHIC_CHARGED" # RHIC | RHIC_CHARGED | LHC
OUTDIR="DataOut" #output directory name
JET_TYPE="pythia" # pythia 
export PTCUT=0.2 #min particle pT
export PTMAX=30 #max particle pT
export JETONLY=1 #only hard jets, if JETONLY=0 and BOLTZMAN=0 => generate hard jets + BG
export BOLTZMANN=0 #only soft BG
export BKGDPTCUT=0.2 #not used anymore
export HARDJET_TYPE=2 # hard jet spectrum distribution: 0: pT^-6 | 1: unbiased full PYTHIA R=0.6 (double Levy fit) | 2: unbiased full PYTHIA R=0.6 (Tsalis fit)
export PTMINHARD=4 #start of the hard distribution, default: 3GeV/c
EFFICORR=1 #simulate tracking inefficiency
PTSMEAR=1 #track pT smearing
export EFFTYPE="pp" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies
export TOFEFFI=0; #apply TOF efficiency as one of the detector effects (this is used only for pp RM production)
export DOSCALE=0 #rescale charge jets to the momentum of created full jet: 0: do not scale | 1: scale momentum of charged jet constituents | 2: create full jet with 3/2*pT
export RAAPTDEP=1 #pT dependent RAA - RAA starts at 0.2 and then slowly rises (upt to pT=15GeV)  to the value of "RAA"
export JETFRAG="u" #jet fragmentation: u | g | sp
export MEANPT=600 #Background <pT> [MeV/c]
MULTIPLICITY=650 #Background multiplicity

for CENTRAL in 1 #central or peripheral collisions
do
export CENTRAL 
if [ $CENTRAL -eq 1 ]; then
	RAA=0.5
else
	RAA=0.7
	RAAPTDEP=0
fi
export RAA

if [ $RAAPTDEP -eq 1 ]; then
	RAATYPE="varRAA"
else
	RAATYPE="RAA"
fi

if [ $CENTRAL -eq 1 ]; then
CSUF=""
else
CSUF="_peripheral"
fi

for RADIUS in 0.3 #0.2 0.3 0.4 
do
export RADIUS 

TYPE4="_ETA${CSUF}_${RAATYPE}${RAA}_pTmax${PTMAX}_pTHARD${PTMINHARD}_${JETFRAG}_meanpT${MEANPT}_mult${MULTIPLICITY}" #output directory suffix


	if [ $JETONLY -eq 1 ]; then
		EVENT_TYPE="jetonly"
	elif [ $BOLTZMANN -eq 1 ]; then
		EVENT_TYPE="boltzman"
	else
		EVENT_TYPE="jet_plus_bg" 
	fi

   if [ $RADIUS == "0.2" ]; then
      ACUT=0.09
   elif [ $RADIUS == "0.3" ]; then
      ACUT=0.2
   elif [ $RADIUS == "0.4" ]; then
      ACUT=0.4
	elif [ $RADIUS == "0.5" ]; then
      ACUT=0.65
   fi

#if ([ $JETONLY -eq 1 ] || [ $BOLTZMANN -eq 1 ]); then
	#EFFICORR=0 
	#PTSMEAR=0 
	#ACUT=0
#fi
export ACUT
export EFFICORR
export PTSMEAR

echo "Area cut: $ACUT"

	if [ $HARDJET_TYPE -eq 0 ]; then
	TYPE1="${MEVENTS}M_charged_R${RADIUS}_A${ACUT}_powlaw"
	elif [ $HARDJET_TYPE -eq 1 ]; then
	TYPE1="${MEVENTS}M_charged_R${RADIUS}_A${ACUT}_levy"
	elif [ $HARDJET_TYPE -eq 2 ]; then
	TYPE1="${MEVENTS}M_charged_R${RADIUS}_A${ACUT}_tsalis"
	fi

	if [ $EFFICORR -eq 1 ]; then
	TYPE2="_effcorr"
	else
	TYPE2=""
	fi

	if [ $PTSMEAR -eq 1 ]; then
	TYPE3="_pTsmear"
	else
	TYPE3=""
	fi

	TYPE=${TYPE1}${TYPE2}${TYPE3}${TYPE4}

	export NEVENTS=25000 #how many events per job
	START=0
	MAX=$(( MEVENTS * 1000000 / NEVENTS ))
	#START=75
	#MAX=8
	echo "number of jobs: $MAX"

	if [ $CENTRAL -eq 1 ]; then
     NBIN=955 #0-10% centrality
     #NBIN=984 #0-5% centrality, Gabriel's value
     #NBIN=1043 #0-5% centrality, Jan's value
     #MULTIPLICITY=600 #0-10% centrality, charged tracks only
	  #MULTIPLICITY=1333 #0-5% centrality, charged tracks only, Gabriel's value
     #MULTIPLICITY=1410 #0-5% centrality, charged tracks only, Jan's value
   else
	  NBIN=20 #60-80%	
     #MULTIPLICITY=40
	fi

     export NBIN
	  export SIGMA_NBIN=0
	  export MULTIPLICITY
	  export SIGMA_MULTIPLICITY=0
  
	  for((run=START; run < MAX; run++))
   	 do
	    export NAME="toy${COLLIDER}${run}_${JET_TYPE}_${TYPE}"
	    export OUTPUTDIR="${BASEDIR}/$OUTDIR/${JET_TYPE}/${EVENT_TYPE}/${TYPE}/${run}"
	    #export OUTPUTDIR="${GSCRATCH}/${JET_TYPE}/${EVENT_TYPE}/${TYPE}/${run}"
 
  	    if [ ! -e $OUTPUTDIR ]; then
			mkdir -p $OUTPUTDIR
	    fi
		if [ $ANAONLY -eq 1 ]; then
   	 	qsub -l projectio=1 -l h_rt=00:30:00 -m n -N $NAME -e $LOGDIR -o $LOGDIR -V run_inclusive_${JET_TYPE}.csh
		else
   	 	qsub -l projectio=1 -m n -N $NAME -e $LOGDIR -o $LOGDIR -V run_inclusive_${JET_TYPE}.csh
		fi
	  done

done #R
done #central
