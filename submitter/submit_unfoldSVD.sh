#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BG_dete | dete | effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage


prior_type=(truth flat pythia powlaw3 powlaw45 powlaw5 powlaw55 levy levy_alex )
export PTCUT=0.2
export NBINS=VAR
COLLIDER="RHIC" # "LHC" # 
JET_TYPE="pythia" # sp | pythia
export SVD=1
# FOR PRIOR DISTRIBUTION
export NITER=10 #number of k-terms
EFFICORR=1 #do efficiency correction
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
export INPUTITER=5 #if unfolding already unfolded results, which iteration to unfold
export EFFI_UNFOLD=0 #for jet reconstruction efficiency calculation
CENTRAL=1 #central/peripheral collisions
RAA=0.2 #RAA value used for generating the simulated spectrum - used only for desctription
SUF="_RMpriorscale" #output directory suffix

if [ $CENTRAL -eq 1 ]; then
	CENTSUFF=""
	PTLEADCUTS="5 7"
else
	CENTSUFF="_peripheral"
	PTLEADCUTS="3 4 5"
fi

if [ $EFFI_UNFOLD -eq 1 -o $RMATRIX_TYPE == "BG_sp" ]; then
   export EFFICORR=0
else
   export EFFICORR
fi

if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

for BININGCH in 1 2 3 4 #choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
do
export BININGCH 

for RPARAM in 0.4 #0.4
do
export RPARAM
TOYMODELPATH="$HOME/jet_analysis/toymodel"
WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}${CENTSUFF}"
export TRUE_PATH="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/charged_R${RPARAM}${CENTSUFF}"
export PRIOR_PATH="~/jet_analysis/STARJet/out/MB/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH="$WRKDIR/rmatrix"
export RMATRIX_TYPE
export EPSILON_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}_normal/rmatrix"
LOGDIR="$TOYMODELPATH/submitter/log"

for PRIOR in 2 4 5 6 7 8 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:(pT-6) 7: levy 8: levy II
do
   OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_SVD_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA}${SUF}/"${prior_type[$PRIOR]}
   echo "creating directory: $OUT_DIR"
   rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
	export OUT_DIR
   export PRIOR
   for PTTHRESH in `echo $PTLEADCUTS`
   do
     export PTTHRESH
     NAME="unfoldingSVD_${PRIOR}_$PTTHRESH"

		qsub -P star -m n -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_unfoldSVD.sh
	done #prior
done #pTlead
done #R
done #bining
