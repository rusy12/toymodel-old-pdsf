#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load toymodel/toymodel


#BASEDIR=$HOME/jet_analysis/toymodel
#SCRIPTS=$BASEDIR/softscripts
MacroDir=$BASEDIR/macros

export INPUTDIR=$OUTPUTDIR
export WRKDIR=$OUTPUTDIR

cd $MacroDir
root -b -q -l produce_data.C  #2>&1
root -b -q -l make_dNdpT.C  #2>&1
root -b -q -l rec_jets.C  #2>&1
root -b -q -l make_ntuple.C  #2>&1
root -b -q -l make_histos.C  #2>&1
root -b -q -l make_histos_deltapT.C #2>&1
#done
