#include "TLorentzVector.h"

#include "ThrmJet.h"
#include "ThrmEvent.h"
#include "ThrmFourVector.h"
#include "ThrmAnaJetByJet.h"

#include "Riostream.h"

//_____________________________________________________________________________
ThrmAnaJetByJet::ThrmAnaJetByJet(TString wrkdir, Double_t pTcut, Double_t radius)
{
  TString str = "null";

  // TOY MODEL EVENTS
  str = Form("%s/toymodel_events.root", wrkdir.Data());
  ftoymodel = new TFile(str.Data(), "OPEN");
  ttoymodel = (TTree*)ftoymodel->Get("ToyModel");
  ttoymodel->SetBranchAddress("particles", &partarr);

  // JET EVENTS
  str = Form("%s/jets_R%.1lf_pTcut%.1lf.root", wrkdir.Data(), radius, pTcut);
  fjets = new TFile(str.Data(), "OPEN");
  tjets = (TTree*)fjets->Get("RecoJets");
  tjets->SetBranchAddress("akt_jets", &aktjetarr);

  // OUTPUT
  str = Form("%s/histos_jetbyjet.root", wrkdir.Data());
  foutput = new TFile(str.Data(), "RECREATE");
  CreateHistos();
}

//_____________________________________________________________________________
ThrmAnaJetByJet::~ThrmAnaJetByJet()
{
  for(Int_t ihist = 0; ihist < 5; ihist++)
    delete hfraction[ihist];
  delete hetaJetHadron;
  
  foutput->Close();
  delete foutput;

  fjets->Close();
  delete fjets;

  ftoymodel->Close();
  delete ftoymodel;
}

//_____________________________________________________________________________
void ThrmAnaJetByJet::CreateHistos()
{
  TString name = "null";
  TString title = "null";

  // WITHIN ACCEPTANCE?
  for(Int_t ihist = 0; ihist < 5; ihist++)
    {
      name = Form("hfraction_pT%.1lf", (Float_t)(ihist + 1));
      title =  Form("%s;Within acceptance?;Fraction", name.Data());
      hfraction[ihist] = new TH1D(name.Data(), title.Data(), 10, -1, +2);
      hfraction[ihist]->Sumw2();
    }

  // ETA_JET x ETA_HADRON
  name = "hetaJetHadron";
  title =  Form("%s;#eta_{hadron};#eta_{jet}", name.Data());
  hetaJetHadron = new TH2D(name.Data(), title.Data(), 100, -1.0, +1.0, 100, -1.0, +1.0);
}

//_____________________________________________________________________________
void ThrmAnaJetByJet::RunAnalysis()
{
  Int_t nevents = ttoymodel->GetEntries();

  for(Int_t entry = 0; entry < nevents; entry++)
    {
      ttoymodel->GetEntry(entry);
      tjets->GetEntry(entry);

      Int_t njets = aktjetarr->GetEntries();

      if(entry % 100 == 0)
	cout << Form("Event #%d, %d jets analyzed", entry, njets) << endl;

      for(Int_t ijet = 0; ijet < njets; ijet++)
	{
	  ThrmJet *jet = (ThrmJet*)aktjetarr->At(ijet);
	  ThrmFourVector jetfv = jet->jet_fv;
	  TLorentzVector jetlv = jetfv.GetTLorentzVector();
	  Int_t hadron_idx = jet->embeddedJet_idx;
	  
	  ThrmFourVector *partfv = (ThrmFourVector*)partarr->At(hadron_idx);
	  TLorentzVector partlv = partfv->GetTLorentzVector();

	  Double_t areajet = jet->area;
	  Double_t etajet = jetlv.Eta();
	  Double_t etahadron = partlv.Eta();
	  Double_t pThadron = partlv.Pt();
	  if(areajet < 0.4 || TMath::Abs(etahadron) > 0.6) continue;

	  hetaJetHadron->Fill(etahadron, etajet);

	  for(Int_t ihist = (Int_t)pThadron; ihist < 5; ihist++)
	    {
	      if(TMath::Abs(etajet) < 0.6 && TMath::Abs(etahadron) < 0.6)
		hfraction[ihist]->Fill(1);
	      else
		hfraction[ihist]->Fill(0);
	    }
	}
    }
  Terminate();
}

//_____________________________________________________________________________
void ThrmAnaJetByJet::Terminate()
{
  for(Int_t ihist = 0; ihist < 5; ihist++)
    {
      if(hfraction[ihist]->GetEntries())
	hfraction[ihist]->Scale(1./hfraction[ihist]->GetEntries());
      hfraction[ihist]->Write();
    }

  hetaJetHadron->Write();
}
