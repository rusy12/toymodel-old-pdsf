#ifndef __ThrmAnaJetByJet__hh
#define __ThrmAnaJetByJet__hh

#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TNtuple.h"
#include "TClonesArray.h"

class ThrmAnaJetByJet
{
 public:
  ThrmAnaJetByJet(TString wrkdir, Double_t pTcut, Double_t radius);
  ~ThrmAnaJetByJet();
  
  void RunAnalysis();

 private:
  void Terminate();
  void CreateHistos();

 protected:
  TFile *fjets;
  TFile *foutput;
  TFile *ftoymodel;

  TTree *tjets;
  TTree *ttoymodel;

  TClonesArray *partarr;
  TClonesArray *aktjetarr;

  TH1D *hfraction[5];
  TH2D *hetaJetHadron;
};

#endif
