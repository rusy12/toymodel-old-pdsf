#ifndef __ThrmAnaDeltaPtArea__hh
#define __ThrmAnaDeltaPtArea__hh

class TH1D;
class TH2D;
class TFile;
class TTree;
class TClonesArray;

class ThrmAnaDeltaPtArea
{
 public:
  ThrmAnaDeltaPtArea(TString path, Double_t pTcut, Double_t radius, Double_t areacut);
  ~ThrmAnaDeltaPtArea();

  void FillHistos();

 protected:
  void CreateHistos();

 private:
  TH1D *fhdpT[20];
  TH2D *fhdpTpTlead_Acut[20];
  TH2D *fhdpTArea[20];

  TTree *ftree;

  TFile *finput;
  TFile *foutput;

  TClonesArray *fembedding_arr;
 
  Double_t fAcut;
};

#endif
