#ifndef __UnfoldSVD__hh
#define __UnfoldSVD__hh
 
#include "Rtypes.h"
#include "TString.h"

class TSVDUnfold;
class TH1D;
class TH2D;
class TFile;

class UnfoldSVD
{
 public:
  UnfoldSVD(Int_t kterm, TString outfile);
  ~UnfoldSVD();

  void Unfold();
  void SetHistograms(TH1D *measured,TH1D* reco_prior, TH1D *prior, TH2D *response_matrix);

 private:

 protected:	
  TFile *fout;
  Int_t fKterm;		//cut-off term

const  TH1D *HPrior;
const  TH1D *HRecoPrior;
const  TH1D *HMeasured;
const  TH2D *HResponse;
  TH1D *HUnfolded;
  TH1D *HDvector;
  TH1D *HSingVal;
  TH2D *HCovData;
  TH2D *HCovUnf;
  TH2D *HCovTau;

};

#endif
