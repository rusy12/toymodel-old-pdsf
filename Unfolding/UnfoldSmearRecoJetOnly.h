#ifndef __UnfoldSmearRecoJetOnly__hh
#define __UnfoldSmearRecoJetOnly__hh

void SmearRecoJetOnly(Double_t pTcut = 0.2, Double_t pTthresh = 4.0);

#endif
