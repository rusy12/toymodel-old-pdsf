#ifndef __UnfoldFolding__hh
#define __UnfoldFolding__hh

#include "TH1D.h"

void FoldWithDeltaPt(TH1D *truth, TH1D **hdpT, TH1D *smeared);

#endif

