#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TString.h"
#include "TRandom.h"

#include "UnfoldBayes.h"
#include "UnfoldExamples.h"

void TestPlaw(Int_t niterations, Double_t mean, Double_t sigma)
{
  TString str;

  Int_t nbins = 100;
  Int_t nevts = 1E7;
  Double_t xmin = -50;
  Double_t xmax = +50;

  gRandom->SetSeed(0);

  str = Form("./OutputFiles/plaw_mean%.1lf_sigma%.1lf.root", mean, sigma);
  UnfoldBayes *bayes = new UnfoldBayes(niterations, str.Data());

  TH1D *htruth = new TH1D("htruth","htruth;p_{T} (GeV/c);entries", nbins, xmin, xmax);
  htruth->SetLineColor(kBlack);
  htruth->SetLineWidth(2);

  TH1D *hmeasured = new TH1D("hmeasured","hmeasured;p_{T} (GeV/c);entries", nbins, xmin, xmax);
  hmeasured->SetLineColor(kRed);
  hmeasured->SetMarkerColor(kRed);
  hmeasured->SetMarkerStyle(kFullCircle);

  TH1D *hprior = new TH1D("hprior","hprior;x;entries", nbins, xmin, xmax);
  hprior->SetLineColor(kBlue);

  TH2D *hresponse = new TH2D("hresponse","hresponse;x_{T}^{truth};p_{T}^{measured};entries", nbins, xmin, xmax, nbins, xmin, xmax);
  
  // TRUTH SPECTRUM
  TF1 *ftruth = new TF1("spectrum", "[0]/x**[1] * 1./(1 + TMath::Exp(-(x - [2])/[3]))", 1., 100.);
  Double_t power = 6.0;
  Double_t pTcut = 4.0;
  Double_t width = 1E-10;
  ftruth->SetParameters(1, power, pTcut, width);
  Double_t Njets = nevts * 1E+5*1E-12 * 1E+2 / (42*1E-3); // ppjet_xsec * nbin / sigma_proc
  ftruth->SetParameter(0, Njets/ftruth->Integral(10,100));

  // SMEARING FUNCTION
  TF1 *gausn = new TF1("gausn","gausn", xmin, xmax);
  gausn->SetNpx(nbins);
  
  // MEASURING
  for(Int_t bin = 1; bin <= nbins; bin++)
    {
      Double_t pT = htruth->GetBinCenter(bin);
      if(pT < 1.) continue;
      
      htruth->SetBinContent(bin, ftruth->Eval(pT));
    }

  //  SMEARING
  for(Int_t bin = 1; bin <= nbins; bin++)
    {
      gausn->SetParameters(1, htruth->GetBinCenter(bin) + mean, sigma);
      TH1D *hgausn = (TH1D*)gausn->GetHistogram();
      hgausn->SetName("hgausn");
      
      hgausn->Scale(htruth->GetBinContent(bin));
      hmeasured->Add(hgausn);
    }  

  // REMOVING COUNTS < 1
  TH1D *htemp = (TH1D*)hmeasured->Clone("htemp");
  hmeasured->Reset("MICE");
  for(Int_t bin = 1; bin <= hmeasured->GetNbinsX(); bin++)
    {
      Double_t counts = htemp->GetBinContent(bin);
      if(counts < 1) continue;
	
      counts = gRandom->Poisson(counts);
      Double_t error = TMath::Sqrt(counts);
	
      hmeasured->SetBinContent(bin, counts);
      hmeasured->SetBinError(bin, error);
    }
  delete htemp;

  // MAPPING CAUSE => EFFECT
  for(Int_t ievt = 0; ievt < nevts; ievt++)
    {
      Double_t pTtruth = gRandom->Uniform(1., 100.);
      Double_t pTmeasured = pTtruth + gRandom->Gaus(mean, sigma);
      hresponse->Fill(pTtruth, pTmeasured);
    }

  // PRIOR
  for(Int_t bin = 1; bin < nbins; bin++)
    hprior->SetBinContent(bin, htruth->GetBinContent(bin));

  bayes->SetHistograms(htruth, hmeasured, hprior, hresponse);
  bayes->Unfold();
  
  delete bayes;
}

