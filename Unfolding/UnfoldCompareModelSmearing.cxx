#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TStopwatch.h"

#include "Riostream.h"

#include "UnfoldBayes.h"
#include "UnfoldCompareModelSmearing.h"

void CompareModelSmearing(Double_t pTcut)
{
  TStopwatch timer;
  timer.Start();

  TString str;

  // DELTA PT
  TFile *fdeltapT = new TFile("./OutputFiles/histos_dpTarea_R0.4_pTcut0.2.root", "OPEN");
  TH1D *hdpT[4];
  Float_t pTemb[] = {1.0, 5.0, 10.0, 15.0};

  for(Int_t iemb = 0; iemb < 4; iemb++)
    {
      hdpT[iemb] = (TH1D*)fdeltapT->Get(Form("hdpT_Acut_pTemb%.1lf", pTemb[iemb]));
      hdpT[iemb]->Rebin(2);
      if(hdpT[iemb]->Integral() != 1.0)
	hdpT[iemb]->Scale(1./hdpT[iemb]->Integral());
      cout << Form("pTemb = %.1lf \t Integral = %.3e", pTemb[iemb], hdpT[iemb]->Integral()) << endl;
    }

  // AVERAGE CORRECTED PT
  TFile *fjets = new TFile("./OutputFiles/histos_jets_R0.4_pTcut0.2.root", "OPEN");
  TH2D *hCorrPtLeading = (TH2D*)fjets->Get("fhDSpTleading");
  Int_t binmin_proj = hCorrPtLeading->GetXaxis()->FindFixBin(pTcut);
  Int_t binmax_proj = hCorrPtLeading->GetNbinsX();
  TH1D *hCorrPt = hCorrPtLeading->ProjectionY("hCorrPt", binmin_proj, binmax_proj, "e");
  //  hCorrPt->Rebin(20);

  // RESPONSE MATRIX
  TFile *frespmatrix = new TFile("./OutputFiles/response_matrix_deltapT_uniform_pTcut0.2.root", "OPEN");
  TH2D *hrespmatrix = (TH2D*)frespmatrix->Get("hResponse_1E9");
  hrespmatrix->Rebin2D(3, 3);

  // TRUTH SPECTRUM
  //  TF1 *fntruth = new TF1("fntruth","[0]*TMath::Power(x[0], [1])/(1 + TMath::Exp(-(x[0] - [2])/[3]))", pTcut, 60);
  TF1 *fntruth = new TF1("fntruth","[0]*TMath::Power(x[0], [1])", pTcut, 60);
  fntruth->SetParNames("Amplitude", "power");
  fntruth->SetParameters(1, -6);
  fntruth->SetParameter(0, hCorrPt->GetEntries()/fntruth->Integral(pTcut, 60));

  // ANALYSIS 
  TFile *foutput = new TFile("./OutputFiles/plaw_deltapT.root", "RECREATE");

  Int_t nbins = hCorrPt->GetNbinsX();
  Double_t xmin = hCorrPt->GetBinLowEdge(1);
  Double_t xmax = hCorrPt->GetBinLowEdge(nbins+1);

  TH1D *hmeasured = new TH1D("hmeasured","hmeasured;p_{T} (GeV/c);entries", nbins, xmin, xmax);
  hmeasured->SetLineColor(kRed);
  hmeasured->SetMarkerColor(kRed);
  hmeasured->SetMarkerStyle(kFullCircle);

  TH1D *htruth = new TH1D("htruth","htruth;p_{T} (GeV/c);entries", nbins, xmin, xmax);
  htruth->SetLineColor(kBlack);
  htruth->SetLineWidth(2);

  TH1D *hprior = new TH1D("hprior","hprior;x;entries", nbins, xmin, xmax);
  hprior->SetLineColor(kBlue);

  TH1D *htemp = new TH1D("htemp","htemp", nbins, xmin, xmax);

  // MEASURING
  for(Int_t bin = 1; bin <= nbins; bin++)
    {
      Double_t pT = htruth->GetBinLowEdge(bin);
      Double_t width = htruth->GetBinWidth(bin);
      if(pT < pTcut) continue;
      
      Double_t yield = fntruth->Integral(pT, pT+width);
      htruth->SetBinContent(bin, yield);
      hprior->SetBinContent(bin, yield);
    }

  hCorrPt->Rebin(20);
  hmeasured->Rebin(20);
  htruth->Rebin(20);
  hprior->Rebin(20);
  htemp->Rebin(20);

  //  SMEARING
  TH1D *hsmear;

  for(Int_t bin = 1; bin <= htemp->GetNbinsX(); bin++)
    {
      htemp->Reset("MICE");
      Int_t deltabin = bin - 50;
      Double_t yield = htruth->GetBinContent(bin);
      Double_t pT = htruth->GetBinCenter(bin);
      if(0)
	TH1D *hsmear = (TH1D*)hdpT[3]->Clone("hsmear");
      if(pT < 1.0)
	hsmear = (TH1D*)hdpT[0]->Clone("hdpT");
      else 
	if (pT < 7.0)
	  hsmear = (TH1D*)hdpT[1]->Clone("hdpT");
	else
	  if (pT < 12)
	    hsmear = (TH1D*)hdpT[2]->Clone("hdpT");
	  else
	    hsmear = (TH1D*)hdpT[3]->Clone("hdpT");
      
      for(Int_t bintemp = 1; bintemp <= nbins; bintemp++)
	{
	  Double_t yield_smeared = yield*hsmear->GetBinContent(bintemp);
	  htemp->SetBinContent(bintemp + deltabin, yield_smeared);
	}

      hmeasured->Add(htemp);
      htemp->Reset("MICE");
    }  
  delete htemp;

  {
    // REMOVING COUNTS < 1
    htemp = (TH1D*)hmeasured->Clone("htemp");
    hmeasured->Reset("MICE");
    for(Int_t bin = 1; bin <= hmeasured->GetNbinsX(); bin++)
      {
	Double_t counts = htemp->GetBinContent(bin);
	if(counts < 1) continue;
	  
	counts = gRandom->Poisson(counts);
	Double_t error = TMath::Sqrt(counts);
	  
	hmeasured->SetBinContent(bin, counts);
	hmeasured->SetBinError(bin, error);
      }
    delete htemp;
  }


  // UNFOLDING
  Int_t niterations = 5;
  UnfoldBayes *bayes = new UnfoldBayes(niterations, "./OutputFiles/unfolded_plaw_deltapT.root");
  bayes->SetHistograms(htruth, hmeasured, hprior, hrespmatrix);
  bayes->Unfold();

  foutput->cd();
  htruth->Write();
  hmeasured->Write();
  hprior->Write();
  hCorrPt->Write();
  foutput->Close();

  fjets->Close();
  frespmatrix->Close();
  fdeltapT->Close();

  timer.Stop();
  timer.Print();
}

