{
  gSystem->Load("./libtoyUnfold.so");

  gROOT->SetStyle("Modern");
  gStyle->SetPalette(1);
  gStyle->SetTextSize(0.04);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptDate(1);


  // CANVAS CONFIGURATION
  gStyle->SetCanvasDefH(768);
  gStyle->SetCanvasDefW(1336);

  gStyle->SetCanvasDefX(0);
  gStyle->SetCanvasDefY(0);

  gStyle->SetPadTopMargin(0.07);
  gStyle->SetPadBottomMargin(0.15);

  // AXIS TITLE
  gStyle->SetTitleSize(0.06, "X");
  gStyle->SetTitleSize(0.06, "Y");

  gStyle->SetLabelSize(0.05, "X");
  gStyle->SetLabelSize(0.05, "Y");

  gStyle->SetTitleOffset(0.8, "X");
  gStyle->SetTitleOffset(0.8, "Y");


  // TH2D CONFIGURATION
  gStyle->SetNumberContours(500);
}
