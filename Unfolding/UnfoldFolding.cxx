#include "UnfoldFolding.h"

//_____________________________________________________________________________
void FoldWithDeltaPt(TH1D *truth, TH1D **hdpT, TH1D *smeared)
{
  Int_t nbins = smeared->GetNbinsX();
  Double_t xmin = smeared->GetBinLowEdge(1);
  Double_t xmax = smeared->GetBinLowEdge(nbins+1);

  TH1D *hsmear;
  TH1D *htemp = new TH1D("htemp", "htemp", nbins, xmin, xmax);
  
  for(Int_t bin = 1; bin <= htemp->GetNbinsX(); bin++)
    {
      htemp->Reset("MICE");
      Int_t deltabin = bin - 50;
      Double_t deltapT = truth->GetBinCenter(bin - 50);
      Double_t yield = truth->GetBinContent(bin);
      Double_t pT0 = truth->GetBinCenter(bin);
      
      if (pT0 < 3.0)
	hsmear = (TH1D*)hdpT[0]->Clone("hdpT");
      else
	if (pT0 < 7.0)
	  hsmear = (TH1D*)hdpT[1]->Clone("hdpT");
	else
	  if (pT0 < 12)
	    hsmear = (TH1D*)hdpT[2]->Clone("hdpT");
	  else
	    hsmear = (TH1D*)hdpT[3]->Clone("hdpT");

      Double_t pTmean = hsmear->GetMean();

      for(Int_t binsmear = 1; binsmear <= hsmear->GetNbinsX(); binsmear++)
	{
	  Double_t yield_smeared = yield*hsmear->GetBinContent(binsmear);
	  Double_t pTR = hsmear->GetBinCenter(binsmear);
	  htemp->Fill(pT0 + pTR, yield_smeared);
	}
      
      smeared->Add(htemp);
      htemp->Reset("MICE");
    }  
  delete htemp;

}
