#!/bin/csh
set STARLIB_VER = SL15e
set BASEPATH = /global/homes/r/rusnak/jet_analysis/STARJet
setenv STARJETBASEDIR $BASEPATH 
#setenv ANALYSISDIR $BASEPATH/analysis_$STARLIB_VER
#setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ANALYSISDIR

setenv ROOUNFOLD $BASEPATH/software_$STARLIB_VER/RooUnfold/v-trunk-custom
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ROOUNFOLD

setenv FASTJETDIR /global/homes/r/rusnak/jet_analysis/STARJet/software_$STARLIB_VER/fastjet3
setenv PATH $PATH\:$FASTJETDIR/bin
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$FASTJETDIR/lib

set TOYPATH = /global/homes/r/rusnak/jet_analysis/toymodel
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYPATH/Production
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYPATH/Analysis
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYPATH/Unfolding
