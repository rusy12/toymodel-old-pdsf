#include "TH2D.h"
#include "TFile.h"
#include "TSystem.h"
#include "TVector2.h"
#include "TPythia6.h"
#include "TParticle.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"

#include "Riostream.h"

#include "ThrmTestFragmentation.h"

#include "fjwrapper.h"
#include <fastjet/PseudoJet.hh>

void TestFragLEP()
{
  TClonesArray *simarr = new TClonesArray("TParticle", 1000);
  TLorentzVector partlv;

  TPythia6 *pythia = new TPythia6();

  TFile *foutput = new TFile("./fraglep.root", "RECREATE");
  TH2D *hpTjetpTleading = new TH2D("hpTjetpTleading","pTjet X pTleading;p_{T}^{leading} (GeV/c);p_{T}^{jet}", 100, 0, 100, 100, 0, 100);
  TH2D *hpTjetpTsim = new TH2D("hpTjetpTsim","pTjet X pTsim;p_{T}^{sim} (GeV/c);p_{T}^{jet}", 100, 0, 100, 100, 0, 100);

  for(Double_t pT = 12.; pT < 200; pT += 1.)
    for(Int_t i = 0; i < 10; i++)
      {
	Bool_t found = kFALSE;
	pythia->Initialize("cms", "e+", "e-", pT);
	pythia->SetMSEL(1);
	pythia->SetMSUB(11, 1);
	pythia->SetMSUB(12, 1);
	pythia->SetMSUB(13, 1);
	cout << Form("pT= %.1lf", pT) << endl;
	while(!found)
	  {
	    simarr->Delete();
	
	    pythia->GenerateEvent();
	
	    Int_t final = pythia->ImportParticles(simarr, "Final");
	    Int_t nparticles = simarr->GetEntries();
	
	    std::vector<fastjet::PseudoJet> input_data;
	    for(Int_t ipart = 0; ipart < nparticles; ipart++)
	      {
		TParticle *particle = (TParticle*)simarr->At(ipart);
		Int_t pdg = TMath::Abs(particle->GetPdgCode());
	    
		if(pdg == 12 || pdg == 14 || pdg == 16 || pdg == 18 ||
		   pdg == 2112 || pdg == 130) continue;
	    
		particle->Momentum(partlv);
	    
		if(partlv.Pt() < 0.2) continue;
	    
		if(TMath::Abs(partlv.Eta()) > 1.0) continue;
	    
		fastjet::PseudoJet inp_particle(partlv.Px(),
						partlv.Py(),
						partlv.Pz(),
						partlv.Energy());
	    
		input_data.push_back(inp_particle);
	      } 
	
	    FJWrapper akt_data;
	    akt_data.r = 0.4;
	    akt_data.maxrap = 1.;
	    akt_data.algor = fastjet::antikt_algorithm;
	    akt_data.input_particles = input_data;
	    akt_data.Run();

	    // GETTING HARDEST JET
	    std::vector<fastjet::PseudoJet> jets = sorted_by_pt(akt_data.inclusive_jets);      
	    if(!jets.size()) continue;
	    Double_t eta = jets[0].eta();
	    if(TMath::Abs(eta) > 0.6) continue;

	    std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[0]));

	    hpTjetpTleading->Fill(constituents[0].perp(), jets[0].perp());
	    hpTjetpTsim->Fill(pT, jets[0].perp());

	    found = kTRUE;
	  }
      }
  
  foutput->Write();
  delete foutput;
}

void TestFragIndependent()
{
  TClonesArray *simarr = new TClonesArray("TParticle", 1000);
  TLorentzVector partlv;

  TPythia6 *pythia = new TPythia6();

  TFile *foutput = new TFile("./fragindep.root", "RECREATE");
  TH2D *hpTjetpTleading = new TH2D("hpTjetpTleading","pTjet X pTleading;p_{T}^{leading} (GeV/c);p_{T}^{jet}", 100, 0, 100, 100, 0, 100);
  TH2D *hpTjetpTsim = new TH2D("hpTjetpTsim","pTjet X pTsim;p_{T}^{sim} (GeV/c);p_{T}^{jet}", 100, 0, 100, 100, 0, 100);

  for(Double_t pT = 1.; pT < 200; pT += 1.)
    for(Int_t i = 0; i < 10; i++)
      {
	Bool_t found = kFALSE;
	cout << Form("pT= %.1lf", pT) << endl;
	while(!found)
	  {
	    simarr->Delete();
	
	    pythia->Py1ent(1, 2, pT, TMath::Pi()/2., 0);
	    pythia->Pyexec();
	
	    Int_t final = pythia->ImportParticles(simarr, "Final");
	    Int_t nparticles = simarr->GetEntries();
	
	    std::vector<fastjet::PseudoJet> input_data;
	    for(Int_t ipart = 0; ipart < nparticles; ipart++)
	      {
		TParticle *particle = (TParticle*)simarr->At(ipart);
		Int_t pdg = TMath::Abs(particle->GetPdgCode());
	    
		if(pdg == 12 || pdg == 14 || pdg == 16 || pdg == 18 ||
		   pdg == 2112 || pdg == 130) continue;
	    
		particle->Momentum(partlv);
	    
		if(partlv.Pt() < 0.2) continue;
	    
		if(TMath::Abs(partlv.Eta()) > 1.0) continue;
	    
		fastjet::PseudoJet inp_particle(partlv.Px(),
						partlv.Py(),
						partlv.Pz(),
						partlv.Energy());
	    
		input_data.push_back(inp_particle);
	      } 
	
	    FJWrapper akt_data;
	    akt_data.r = 0.4;
	    akt_data.maxrap = 1.;
	    akt_data.algor = fastjet::antikt_algorithm;
	    akt_data.input_particles = input_data;
	    akt_data.Run();

	    // GETTING HARDEST JET
	    std::vector<fastjet::PseudoJet> jets = sorted_by_pt(akt_data.inclusive_jets);      
	    if(!jets.size()) continue;
	    Double_t eta = jets[0].eta();
	    if(TMath::Abs(eta) > 0.6) continue;

	    std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[0]));

	    hpTjetpTleading->Fill(constituents[0].perp(), jets[0].perp());
	    hpTjetpTsim->Fill(pT, jets[0].perp());

	    found = kTRUE;
	  }
      }

  foutput->Write();
  delete foutput;
}

void MakeFragmentationFunction()
{
  Double_t pTmin = atof(gSystem->Getenv("PTMIN"));
  Double_t pTmax = atof(gSystem->Getenv("PTMAX"));

  TString wrkdir = gSystem->Getenv("WRKDIR");

  TClonesArray *simarr = new TClonesArray("TParticle", 1000);
  TLorentzVector partlv;

  TPythia6 *pythia = new TPythia6();
  pythia->SetCKIN(3, pTmin);
  pythia->SetCKIN(4, pTmax);
  
  pythia->SetMSEL(1);
  pythia->SetMSUB(11, 1);
  pythia->SetMSUB(12, 1);
  pythia->SetMSUB(13, 1);
  pythia->SetMSUB(28, 1);
  pythia->SetMSUB(53, 1);
  pythia->SetMSUB(68, 1);
  pythia->SetMSUB(96, 1);

  pythia->Initialize("cms", "p", "p", 200);

  TFile *foutput = new TFile(Form("%s/fragfunc.root", wrkdir.Data()), "RECREATE");
  TH1D *hfrac = new TH1D("hfrac","hfrac;x=(pTleading/pTjet);entries", 150, 0, 1.5);
  hfrac->Sumw2();
  TH2D *hfracpTjet = new TH2D("hfracpTjet","hfrac;pTjet;x=(pTleading/pTjet)", 100, 0, 100, 150, 0, 1.5);
  TH2D *hdRpTjet = new TH2D("hdRpTjet","hdR2pTjet;pTjet;dR", 100, 0, 100, 100, 0, 1.0);
  TH2D *hNpartpTjet = new TH2D("hNpartpTjet","hNpartpTjet;pTjet;Npart", 100, 0, 100, 100, 0, 100);

  for(Int_t ievt = 0; ievt < 1E4; ievt++)
    {
      if(ievt%100 == 0) 
	cout << Form("Event #%d processed", ievt) << endl;
      simarr->Delete();

      pythia->GenerateEvent();
      Int_t final = pythia->ImportParticles(simarr, "Final");
      Int_t nparticles = simarr->GetEntries();
	
      std::vector<fastjet::PseudoJet> input_data;
      for(Int_t ipart = 0; ipart < nparticles; ipart++)
	{
	  TParticle *particle = (TParticle*)simarr->At(ipart);
	  Int_t pdg = TMath::Abs(particle->GetPdgCode());
	    
	  if(pdg == 12 || pdg == 14 || pdg == 16 || pdg == 18 ||
	     pdg == 2112 || pdg == 130) continue;
	    
	  particle->Momentum(partlv);
	    
	  if(partlv.Pt() < 0.2) continue;
	    
	  if(TMath::Abs(partlv.Eta()) > 1.0) continue;
	    
	  fastjet::PseudoJet inp_particle(partlv.Px(),
					  partlv.Py(),
					  partlv.Pz(),
					  partlv.Energy());
	    
	  input_data.push_back(inp_particle);
	} 
	
      FJWrapper akt_data;
      akt_data.r = 0.4;
      akt_data.maxrap = 1.;
      akt_data.algor = fastjet::antikt_algorithm;
      akt_data.input_particles = input_data;
      akt_data.Run();
  
      std::vector<fastjet::PseudoJet> jets = sorted_by_pt(akt_data.inclusive_jets);      
      Int_t njets = jets.size();
      if(!njets) continue;
      
      for(Int_t ijet = 0; ijet < njets; ijet++)
	{
	  Double_t eta = jets[ijet].eta();
	  if(TMath::Abs(eta) > 0.6) continue;
	  
	  std::vector<fastjet::PseudoJet> constituents = sorted_by_pt(akt_data.clust_seq->constituents(jets[ijet]));

	  Int_t nparticles = constituents.size();
	  hNpartpTjet->Fill(jets[ijet].perp(), nparticles);
	  for(Int_t ipart = 0; ipart < nparticles; ipart++)
	    {
	      Double_t x = constituents[ipart].perp()/jets[ijet].perp();
	      hfrac->Fill(x);
	      hfracpTjet->Fill(jets[ijet].perp(), x);
	      
		Double_t dphi2 = TMath::Power(TVector2::Phi_mpi_pi(constituents[ipart].phi() - jets[ijet].phi()), 2.);
	      Double_t deta2 = TMath::Power(constituents[ipart].eta() - jets[ijet].eta(), 2.);
	      Double_t dR = TMath::Sqrt(dphi2 + deta2);
	      hdRpTjet->Fill(jets[ijet].perp(), dR);
	    }
	}
    }
  hfrac->Write();
  hdRpTjet->Write();
  hfracpTjet->Write();
  hNpartpTjet->Write();

  delete hfrac;
  delete hdRpTjet;
  delete hfracpTjet;
  delete hNpartpTjet;

  foutput->Close();
  delete foutput;

  delete simarr;
  delete pythia;
}
